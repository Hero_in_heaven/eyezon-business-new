package com.witheyezon.eyezonbusiness.di.module

import com.eratart.data.repository.auth.AuthRepository
import com.eratart.data.repository.prefs.PreferencesRepository
import com.eratart.domain.repository.IAuthRepository
import com.eratart.domain.repository.IPreferencesRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindAuthRepository(repository: AuthRepository): IAuthRepository

    @Binds
    fun bindPreferencesRepository(repository: PreferencesRepository): IPreferencesRepository
}