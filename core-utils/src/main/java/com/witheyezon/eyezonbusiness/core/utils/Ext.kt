package com.witheyezon.eyezonbusiness.core.utils

import android.util.Log
import com.witheyezon.eyezonbusiness.core.constants.StringConstants.EYEZON_TAG

fun Throwable.printError() {
    if (!BuildConfig.DEBUG) return
    printStackTrace()
    Log.e(EYEZON_TAG, this.message.toString())
}