package com.eratart.tools

import android.content.Context
import androidx.core.content.ContextCompat
import com.eratart.tools.api.IResourceManager
import javax.inject.Inject

class ResourceManager @Inject constructor(private val context: Context) : IResourceManager {

    override fun getDrawable(drawableId: Int) = ContextCompat.getDrawable(context, drawableId)

    override fun getString(stringId: Int) = context.getString(stringId)
}