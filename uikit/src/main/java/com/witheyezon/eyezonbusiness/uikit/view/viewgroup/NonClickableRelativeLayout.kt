package com.witheyezon.eyezonbusiness.uikit.view.viewgroup

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout

class NonClickableRelativeLayout(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {

    init {
        setOnClickListener {  }
    }
}