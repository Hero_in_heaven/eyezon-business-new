package com.witheyezon.eyezonbusiness.uikit.utils

import com.witheyezon.eyezonbusiness.uikit.R
import kotlin.random.Random

class UserLogoChooser {
    companion object {
        private const val RANGE_FROM = 1
        private const val RANGE_TO = 8


        private const val VALUE_1 = 1
        private const val VALUE_2 = 2
        private const val VALUE_3 = 3
        private const val VALUE_4 = 4
        private const val VALUE_5 = 5
        private const val VALUE_6 = 6
        private const val VALUE_7 = 7
        private const val VALUE_8 = 8

        fun getRandomPic(): Int {
            return when (Random.nextInt(RANGE_FROM, RANGE_TO)) {
                VALUE_1 -> R.drawable.user1
                VALUE_2 -> R.drawable.user2
                VALUE_3 -> R.drawable.user3
                VALUE_4 -> R.drawable.user4
                VALUE_5 -> R.drawable.user5
                VALUE_6 -> R.drawable.user6
                VALUE_7 -> R.drawable.user7
                VALUE_8 -> R.drawable.user8
                else -> R.drawable.user1
            }
        }
    }
}