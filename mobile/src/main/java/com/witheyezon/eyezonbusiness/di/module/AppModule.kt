package com.witheyezon.eyezonbusiness.di.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module(includes = [
    NetworkModule::class,
    ToolsModule::class,
    SharedPreferencesModule::class
])
interface AppModule {

    @Binds
    fun provideContext(application: Application): Context

}