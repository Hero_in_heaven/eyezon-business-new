package com.witheyezon.eyezonbusiness.core.constants

object LongConstants{

    const val ZERO = 0L
    const val ONE = 1L
    const val MINUS_ONE = -1L

    const val DURATION_VIBRATION = 10L
    const val DURATION_DEFAULT_GONE = 150L
    const val DURATION_DEFAULT_VISIBLE = 300L
    const val DURATION_DEBOUNCE_CLICK = 500L
    const val DURATION_DEBOUNCE_CHECK_SWITCH = 500L
    const val DURATION_PULSE_RECORDING = 600L
    const val DURATION_SNACK_BAR = 3500L

    const val FINISH_ACTIVITY_DELAY = 150L
}