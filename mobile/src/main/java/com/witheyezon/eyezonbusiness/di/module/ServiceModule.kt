package com.witheyezon.eyezonbusiness.di.module

import com.eratart.data.client.RetrofitBuilder
import com.eratart.data.repository.auth.AuthService
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
class ServiceModule {

    @Provides
    fun provideAuthService(retrofit: RetrofitBuilder): AuthService = retrofit.getAuthService()
}