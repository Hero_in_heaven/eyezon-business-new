package com.witheyezon.eyezonbusiness.feature.main.di.module

import androidx.lifecycle.ViewModel
import com.witheyezon.eyezonbusiness.feature.main.viewmodel.MainViewModel
import dagger.Binds
import dagger.Module

@Module
interface MainActivityModule {

    @Binds
    fun bindsViewModel(viewModel: MainViewModel): ViewModel
}