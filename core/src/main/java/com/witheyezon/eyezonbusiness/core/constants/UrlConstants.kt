package com.witheyezon.eyezonbusiness.core.constants

object UrlConstants  {
    const val BASE_URL_DEV = "https://eyezon.herokuapp.com"
    const val BASE_URL = "https://ourmainserver.witheyezon.com/"

    const val BASE_STREAM_URL = "wss://server.witheyezon.com:8443/"
    const val BASE_STREAM_URL_RECOREDS = "https://server.witheyezon.com:8444/client/records/"
}