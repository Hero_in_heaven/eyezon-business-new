package com.witheyezon.eyezonbusiness.core.constants

object StringConstants  {

    const val EYEZON_TAG = "RE::"
    const val ERROR_RESPONSE_TAG = "Response error"

    const val STAR = "*"
    const val PLUS = "+"
    const val HYPHEN = "-"
    const val UNDERSCORE = "_"

    const val SPACE = " "
    const val EMPTY = ""

    const val DOT = "."
    const val COMMA = ","

    const val BR = "<br>"
    const val NEW_LINE = "\n"
    const val PERCENT = "%"

    const val SLASH = "/"
    const val VERTICAL_LINE = "|"

}