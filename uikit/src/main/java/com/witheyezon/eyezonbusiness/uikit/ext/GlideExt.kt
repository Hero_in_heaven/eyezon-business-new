package com.witheyezon.eyezonbusiness.uikit.ext

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners

fun ImageView.clearImageWithGlide() {
    Glide.with(context)
        .clear(this)
}

fun ImageView.setImageWithGlide(@DrawableRes drawableId: Int) {
    Glide.with(context)
        .load(drawableId)
        .into(this)
}

fun ImageView.setImageWithGlide(drawable: Drawable) {
    Glide.with(context)
        .load(drawable)
        .into(this)
}

fun ImageView.setImageWithGlide(url: String) {
    Glide.with(context)
        .load(url)
        .into(this)
}

fun ImageView.setRoundedImageWithGlide(
    @DrawableRes drawableId: Int, @DimenRes dimen: Int
) {
    Glide.with(context)
        .load(drawableId)
        .transform(RoundedCorners(context.resources.getDimensionPixelSize(dimen)))
        .into(this)
}

fun ImageView.setRoundedImageWithGlide(
    drawable: Drawable, @DimenRes dimen: Int
) {
    Glide.with(context)
        .load(drawable)
        .transform(RoundedCorners(context.resources.getDimensionPixelSize(dimen)))
        .into(this)
}

fun ImageView.setRoundedImageWithGlide(
    url: String, @DimenRes dimen: Int
) {
    Glide.with(context)
        .load(url)
        .transform(RoundedCorners(context.resources.getDimensionPixelSize(dimen)))
        .into(this)
}
