package com.witheyezon.eyezonbusiness.di.module

import com.eratart.tools.ResourceManager
import com.eratart.tools.api.IResourceManager
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ToolsModule {

    @Binds
    fun bindResourceManager(resourceManager: ResourceManager): IResourceManager
}