package com.eratart.data.client.headers

import com.eratart.domain.repository.IPreferencesRepository
import javax.inject.Inject

class HeadersProvider @Inject constructor(preferencesRepository: IPreferencesRepository) :
    IHeadersProvider {

    companion object {
        private const val CONTENT_TYPE = "content-type"
        private const val APPLICATION_JSON = "application/json"

        private const val AUTH_TOKEN = "authToken"
    }

    private val contentType = Pair(CONTENT_TYPE, APPLICATION_JSON)
    private val token = Pair(AUTH_TOKEN, preferencesRepository.getAuthToken())

    override fun getAuthorizedHeaders()  = listOf(contentType, token)

    override fun getNotAuthorizedHeaders() = listOf(contentType)
}