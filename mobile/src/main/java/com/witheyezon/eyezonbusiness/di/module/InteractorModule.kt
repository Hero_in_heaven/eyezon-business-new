package com.witheyezon.eyezonbusiness.di.module

import com.eratart.domain.interactor.auth.AuthInteractor
import com.eratart.domain.interactor.auth.IAuthInteractor
import com.eratart.domain.interactor.preferences.IPreferencesInteractor
import com.eratart.domain.interactor.preferences.PreferencesInteractor
import dagger.Binds
import dagger.Module

@Module
interface InteractorModule {

    @Binds
    fun bindAuthInteractor(interactor: AuthInteractor): IAuthInteractor

    @Binds
    fun bindPreferencesInteractor(interactor: PreferencesInteractor): IPreferencesInteractor
}