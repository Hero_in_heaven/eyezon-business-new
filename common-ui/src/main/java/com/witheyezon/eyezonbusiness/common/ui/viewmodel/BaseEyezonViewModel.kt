package com.witheyezon.eyezonbusiness.common.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eratart.tools.api.IResourceManager
import com.witheyezon.eyezonbusiness.common.ui.extensions.getAction
import com.witheyezon.eyezonbusiness.common.ui.extensions.getConsumer
import com.witheyezon.eyezonbusiness.common.ui.tools.RxTransformers
import com.witheyezon.eyezonbusiness.common.ui.tools.network.NetworkLiveData
import io.reactivex.CompletableTransformer
import io.reactivex.MaybeTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.disposables.Disposable
import javax.inject.Inject

open class BaseEyezonViewModel : ViewModel() {

    @Inject
    lateinit var networkHandler: NetworkLiveData

    @Inject
    lateinit var resourceManager: IResourceManager

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _errorLiveData = MutableLiveData<Throwable>()
    val errorLiveData: LiveData<Throwable>
        get() = _errorLiveData

    private val _errorResponseLiveData = MutableLiveData<Int>()
    val errorResponseLiveData: LiveData<Int>
        get() = _errorResponseLiveData

    private val _authErrorLiveData = MutableLiveData<Boolean>()
    val authErrorLiveData: LiveData<Boolean>
        get() = _authErrorLiveData

    private val _stateLiveData = MutableLiveData<Boolean>()
    val stateLiveData: LiveData<Boolean>
        get() = _stateLiveData

    private val disposableList = ArrayList<Disposable>()

    fun setLogoutState(value: Boolean) {
        _stateLiveData.postValue(value)
    }

    fun handleResponse(code: Int, listener: () -> Unit) {
        when (code) {
            //ResponseCode.CODE_OK -> listener.invoke()
            else -> handleResponseError(code)
        }
    }

    protected fun addDisposable(disposable: Disposable) {
        disposableList.add(disposable)
    }

    protected fun addNotDisposable(disposable: Disposable) {

    }

    fun disposeAll() {
        disposableList.forEach { disposable -> disposable.dispose() }
    }

    private fun setLoading(value: Boolean) {
        _isLoading.postValue(value)
    }

    protected open fun handleError(throwable: Throwable) {
        _errorLiveData.postValue(throwable)
    }

    protected fun handleResponseError(code: Int) {
        when (code) {
            //ResponseCode.CODE_UNAUTHORIZED -> _authErrorLiveData.postValue(true)
            else -> _errorResponseLiveData.postValue(code)
        }
    }

    private fun showLoading() = getConsumer { setLoading(true) }

    private fun hideLoading() = getAction { setLoading(false) }

    protected fun <T> getProgressSingleTransformer(): SingleTransformer<T, T> {
        return RxTransformers.applySingleBeforeAndAfter(showLoading(), hideLoading())
    }

    protected fun <T> getProgressObservableTransformer(): ObservableTransformer<T, T> {
        return RxTransformers.applyObservableBeforeAndAfter(showLoading(), hideLoading())
    }

    protected fun getProgressCompletableTransformer(): CompletableTransformer {
        return RxTransformers.applyCompletableBeforeAndAfter(showLoading(), hideLoading())
    }

    protected fun <T> getProgressMaybeTransformer(): MaybeTransformer<T, T> {
        return RxTransformers.applyMaybeBeforeAndAfter(showLoading(), hideLoading())
    }

}