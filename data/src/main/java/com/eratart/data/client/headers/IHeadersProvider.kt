package com.eratart.data.client.headers

interface IHeadersProvider {

    /**
     * Returns list of headers for authorization endpoints
     */
    fun getNotAuthorizedHeaders(): List<Pair<String, String>>

    /**
     * Returns list of headers for other endpoints
     */
    fun getAuthorizedHeaders(): List<Pair<String, String>>
}