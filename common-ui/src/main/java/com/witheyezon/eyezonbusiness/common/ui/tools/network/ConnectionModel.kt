package com.witheyezon.eyezonbusiness.common.ui.tools.network

data class ConnectionModel(val type: ConnectionType, val status: ConnectionStatus) {
    override fun toString() = "ConnectionModel type: $type , connected: $status"
}