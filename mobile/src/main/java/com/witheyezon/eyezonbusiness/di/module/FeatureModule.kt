package com.witheyezon.eyezonbusiness.di.module

import com.witheyezon.eyezonbusiness.feature.main.view.MainActivity
import com.witheyezon.eyezonbusiness.feature.main.di.component.MainActivitySubComponent
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(
    subcomponents = [
        MainActivitySubComponent::class
    ]
)
interface FeatureModule {

    @Binds
    @IntoMap
    @ClassKey(MainActivity::class)
    fun bindMainActivityFactory(factory: MainActivitySubComponent.Factory): AndroidInjector.Factory<*>
}