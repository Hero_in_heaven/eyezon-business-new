package com.witheyezon.eyezonbusiness.uikit.ext

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.os.Handler
import android.os.HandlerThread
import android.view.LayoutInflater
import android.view.PixelCopy
import android.view.SurfaceView
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.Switch
import com.witheyezon.eyezonbusiness.core.constants.FloatConstants
import com.witheyezon.eyezonbusiness.core.constants.IntConstants
import com.witheyezon.eyezonbusiness.core.constants.LongConstants.DURATION_DEBOUNCE_CHECK_SWITCH
import com.witheyezon.eyezonbusiness.core.constants.LongConstants.DURATION_DEBOUNCE_CLICK
import com.witheyezon.eyezonbusiness.core.constants.LongConstants.DURATION_DEFAULT_GONE
import com.witheyezon.eyezonbusiness.core.constants.LongConstants.DURATION_DEFAULT_VISIBLE
import com.witheyezon.eyezonbusiness.core.constants.LongConstants.DURATION_PULSE_RECORDING
import com.witheyezon.eyezonbusiness.core.constants.PropertyConstants.PROPERTY_NAME_ALPHA
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun <T: View> T.applyPost(block: T.() -> Unit) {
    post { block() }
}

fun View.setVisible(flag: Boolean) {
    if (flag) visible() else gone()
}

fun View.setVisibleAlpha(flag: Boolean) {
    if (flag) visibleWithAlpha() else goneWithAlpha()
}

fun View.visibleWithAlpha(duration: Long = DURATION_DEFAULT_VISIBLE) {
    this.alpha = FloatConstants.ZERO
    this.visible()
    this.animate().alpha(FloatConstants.ONE).setDuration(duration).start()
}

fun View.goneWithAlpha(duration: Long = DURATION_DEFAULT_GONE) {
    this.alpha = FloatConstants.ONE
    this.animate().alpha(FloatConstants.ZERO).setDuration(duration).start()
    Handler().postDelayed({
        this.gone()
    }, duration)
}

fun RelativeLayout.addMatchParentLayoutParams() {
    val params = FrameLayout.LayoutParams(
        FrameLayout.LayoutParams.MATCH_PARENT,
        FrameLayout.LayoutParams.MATCH_PARENT
    )
    this.layoutParams = params
}

fun RelativeLayout.addMatchParentFragmentLayoutParams() {
    val params = RelativeLayout.LayoutParams(
        RelativeLayout.LayoutParams.MATCH_PARENT,
        RelativeLayout.LayoutParams.MATCH_PARENT
    )
    this.layoutParams = params
}

fun View.addScaleAnimation() {
    val scaleDown = android.animation.ObjectAnimator.ofPropertyValuesHolder(
        this,
        android.animation.PropertyValuesHolder.ofFloat(PROPERTY_NAME_ALPHA, FloatConstants.ZERO)
    )
    scaleDown.duration = DURATION_PULSE_RECORDING
    scaleDown.repeatCount = android.animation.ObjectAnimator.INFINITE
    scaleDown.repeatMode = android.animation.ObjectAnimator.REVERSE
    scaleDown.start()
}

fun View.hideKeyboardFrom() {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, IntConstants.ZERO)
}

fun View.rxClickListener(
    duration: Long = DURATION_DEBOUNCE_CLICK,
    timeUnit: TimeUnit = TimeUnit.MILLISECONDS,
    listener: (View) -> Unit
): Disposable {
    return Observable.create<View> { emitter ->
        this.setOnClickListener { emitter.onNext(this) }
    }.throttleFirst(duration, timeUnit)
        .subscribe {
            listener.invoke(this)
        }!!
}

fun Switch.setOnDebounceCheckedChangedListener(
    duration: Long = DURATION_DEBOUNCE_CHECK_SWITCH,
    timeUnit: TimeUnit = TimeUnit.MILLISECONDS,
    listener: (Boolean) -> Unit
): Disposable {
    return Observable.create<Boolean> { emitter ->
        this.setOnCheckedChangeListener { compoundButton, value ->
            if (!compoundButton.isPressed)
                return@setOnCheckedChangeListener
            emitter.onNext(value)
        }
    }
        .debounce(duration, timeUnit)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            listener.invoke(it)
        }
}

fun Int.pxToDp(): Float {
    return this.toFloat() / Resources.getSystem().displayMetrics.density
}

fun Float.pxToDp(): Float {
    return this / Resources.getSystem().displayMetrics.density
}

fun Int.dpToPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun Float.dpToPx(): Float {
    return this * Resources.getSystem().displayMetrics.density
}

fun Context.createView(resId: Int) = LayoutInflater.from(this).inflate(resId, null)

fun SurfaceView.takeScreenshoot(delay: Long = 1000): Observable<Bitmap> {
    return Observable.create<Bitmap> { emitter ->
        Handler().postDelayed({
            val bitmap = Bitmap.createBitmap(
                this.width,
                this.height,
                Bitmap.Config.ARGB_8888
            )
            val handlerThread = HandlerThread("PixelCopier")
            handlerThread.start()
            PixelCopy.request(
                this,
                bitmap,
                { copyResult ->
                    if (copyResult == PixelCopy.SUCCESS) {
                        this.applyPost {
                            emitter.onNext(bitmap)
                            emitter.onComplete()
                        }
                    } else {
                        emitter.onError(Throwable("Cannot create a screenshot"))
                    }
                    handlerThread.quitSafely()
                },
                Handler(handlerThread.looper)
            )
        }, delay)
    }
}

