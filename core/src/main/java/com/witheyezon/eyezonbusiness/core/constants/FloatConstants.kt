package com.witheyezon.eyezonbusiness.core.constants

object FloatConstants {

    const val ZERO = 0f
    const val ONE = 1f
    const val MINUS_ONE = -1f

}