package com.witheyezon.eyezonbusiness.common.ui.tools.network

enum class ConnectionStatus {
    AVAILABLE, UNAVAILABLE
}