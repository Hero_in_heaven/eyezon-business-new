package com.witheyezon.eyezonbusiness.uikit.view.loader

import android.content.Context
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.witheyezon.eyezonbusiness.uikit.R

class DefaultFullscreenLoader(context: Context, attributeSet: AttributeSet? = null) : RelativeLayout(context, attributeSet) {

    init {
        inflate(getContext(), R.layout.loader_view, this)
    }
}