package com.eratart.domain.interactor.preferences

import com.eratart.domain.repository.IPreferencesRepository
import javax.inject.Inject

class PreferencesInteractor @Inject constructor(private val preferencesRepository: IPreferencesRepository) :
    IPreferencesInteractor {

    override fun saveObject(stringKey: String, objectToSave: Any) {
        preferencesRepository.saveObject(stringKey, objectToSave)
    }

    override fun <T : Any> getObject(stringKey: String, defaultValue: T) =
        preferencesRepository.getObject(stringKey, defaultValue)

    override fun getAuthToken() =
        preferencesRepository.getAuthToken()

    override fun saveAuthToken(authToken: String) {
        preferencesRepository.saveAuthToken(authToken)
    }

    override fun getUserId() =
        preferencesRepository.getUserId()

    override fun saveUserId(userId: String) {
        preferencesRepository.saveUserId(userId)
    }
}