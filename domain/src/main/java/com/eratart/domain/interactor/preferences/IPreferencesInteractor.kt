package com.eratart.domain.interactor.preferences

interface IPreferencesInteractor {

    /**
     * Saves any object to shared preferences
     *
     * @param objectToSave is object to save
     */
    fun saveObject(stringKey: String, objectToSave: Any)

    /**
     * Returns object os selected type
     *
     * @param defaultValue is value that returns if an error occurs while getting from preferences
     */
    fun <T : Any> getObject(stringKey: String, defaultValue: T): T?

    fun saveAuthToken(authToken: String)
    fun getAuthToken(): String

    fun saveUserId(userId: String)
    fun getUserId(): String
}