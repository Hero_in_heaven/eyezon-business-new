package com.witheyezon.eyezonbusiness.common.ui.tools.network

enum class ConnectionType {
    WIFI, DATA, UNKNOWN
}