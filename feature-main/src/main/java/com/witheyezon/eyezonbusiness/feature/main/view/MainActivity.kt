package com.witheyezon.eyezonbusiness.feature.main.view

import com.witheyezon.eyezonbusiness.common.ui.activity.BaseEyezonActivity
import com.witheyezon.eyezonbusiness.core.constants.IntConstants
import com.witheyezon.eyezonbusiness.feature.main.viewmodel.MainViewModel
import javax.inject.Inject

class MainActivity : BaseEyezonActivity() {

    @Inject
    lateinit var viewModeL: MainViewModel

    override fun layoutId() = IntConstants.ZERO

    override fun viewModel() = viewModeL
}