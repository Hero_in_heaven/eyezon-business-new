package com.witheyezon.eyezonbusiness.di.module

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import dagger.Module
import dagger.Provides

@Module
class SharedPreferencesModule  {

    companion object {
        private const val SECURE_SHARED_PREFS = "PRIVATE_DATA_EYEZON"
    }

    @Provides
    fun provideEncryptedSharedPreferences(context: Context) : SharedPreferences =
        EncryptedSharedPreferences.create(
            SECURE_SHARED_PREFS,
            MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
}