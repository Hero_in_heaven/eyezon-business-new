package com.witheyezon.eyezonbusiness

import com.witheyezon.eyezonbusiness.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class EyezonApp : DaggerApplication() {

    private val appComponent: AndroidInjector<out DaggerApplication> by lazy {
        DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun applicationInjector() = appComponent

}
