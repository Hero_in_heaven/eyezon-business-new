package com.eratart.tools.api

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

interface IResourceManager {

    fun getDrawable(@DrawableRes drawableId: Int): Drawable?

    fun getString(@StringRes stringId: Int): String
}