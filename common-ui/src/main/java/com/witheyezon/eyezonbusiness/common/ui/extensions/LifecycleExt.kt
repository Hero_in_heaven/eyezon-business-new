package com.witheyezon.eyezonbusiness.common.ui.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T) -> Unit) =
    liveData.observe(this, Observer(body))

fun getAction(listener: () -> Unit) = Action { run { listener.invoke() } }

fun getConsumer(listener: () -> Unit) = Consumer<Disposable> { listener.invoke() }