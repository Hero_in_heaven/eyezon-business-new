package com.eratart.domain.interactor.auth

import com.eratart.domain.repository.IAuthRepository
import javax.inject.Inject

class AuthInteractor @Inject constructor(
    private val authRepository: IAuthRepository
) : IAuthInteractor {
}