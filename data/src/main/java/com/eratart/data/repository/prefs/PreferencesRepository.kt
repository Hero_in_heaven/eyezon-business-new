package com.eratart.data.repository.prefs

import android.content.SharedPreferences
import com.eratart.domain.repository.IPreferencesRepository
import com.google.gson.Gson
import com.witheyezon.eyezonbusiness.core.constants.StringConstants
import com.witheyezon.eyezonbusiness.core.utils.printError
import javax.inject.Inject

class PreferencesRepository @Inject constructor(private val preferences: SharedPreferences) :
    IPreferencesRepository {

    companion object {
        private const val KEY_AUTH_TOKEN = "com.eratart.data.repository.prefs.key.auth.token"
        private const val KEY_USER_ID = "com.eratart.data.repository.prefs.key.user.id"
    }

    override fun saveObject(stringKey: String, objectToSave: Any) {
        val json = Gson().toJson(objectToSave)
        preferences.edit().apply {
            putString(stringKey, json)
            apply()
        }
    }

    override fun <T : Any> getObject(stringKey: String, defaultValue: T): T? {
        val jsonString = preferences.getString(stringKey, StringConstants.EMPTY)
        return try {
            Gson().fromJson(jsonString, defaultValue.javaClass)
        } catch (e: Exception) {
            e.printError()
            defaultValue
        }
    }

    override fun getAuthToken() =
        preferences.getString(KEY_AUTH_TOKEN, StringConstants.EMPTY) ?: StringConstants.EMPTY

    override fun saveAuthToken(authToken: String) {
        preferences.edit().apply {
            putString(KEY_AUTH_TOKEN, authToken)
            apply()
        }
    }

    override fun getUserId() =
        preferences.getString(KEY_USER_ID, StringConstants.EMPTY) ?: StringConstants.EMPTY

    override fun saveUserId(userId: String) {
        preferences.edit().apply {
            putString(KEY_USER_ID, userId)
            apply()
        }
    }
}