package com.witheyezon.eyezonbusiness.feature.main.di.component

import com.witheyezon.eyezonbusiness.feature.main.view.MainActivity
import com.witheyezon.eyezonbusiness.feature.main.di.module.MainActivityModule
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent(modules = [MainActivityModule::class])
interface MainActivitySubComponent : AndroidInjector<MainActivity> {

    @Subcomponent.Factory
    interface Factory : AndroidInjector.Factory<MainActivity>
}