package com.witheyezon.eyezonbusiness.common.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.witheyezon.eyezonbusiness.core.constants.StringConstants
import com.witheyezon.eyezonbusiness.core.constants.StringConstants.ERROR_RESPONSE_TAG
import com.google.android.material.snackbar.Snackbar
import com.witheyezon.eyezonbusiness.common.ui.BuildConfig
import com.witheyezon.eyezonbusiness.common.ui.R
import com.witheyezon.eyezonbusiness.common.ui.extensions.observe
import com.witheyezon.eyezonbusiness.common.ui.tools.network.ConnectionModel
import com.witheyezon.eyezonbusiness.common.ui.tools.network.ConnectionStatus
import com.witheyezon.eyezonbusiness.common.ui.viewmodel.BaseEyezonViewModel
import com.witheyezon.eyezonbusiness.core.utils.printError
import com.witheyezon.eyezonbusiness.uikit.ext.addMatchParentFragmentLayoutParams
import com.witheyezon.eyezonbusiness.uikit.ext.gone
import com.witheyezon.eyezonbusiness.uikit.ext.setVisibleAlpha
import com.witheyezon.eyezonbusiness.uikit.view.loader.DefaultFullscreenLoader
import io.reactivex.disposables.Disposable

abstract class BaseEyezonFragment : Fragment() {

    private val viewModel by lazy { viewModel() }
    private val disposableList = ArrayList<Disposable>()
    private lateinit var fragmentRootView: ViewGroup
    private lateinit var loader: DefaultFullscreenLoader

    abstract fun layoutId(): Int

    abstract fun viewModel(): BaseEyezonViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentRootView = inflater.inflate(layoutId(), null)!! as ViewGroup
        return fragmentRootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLoader()
        viewModel.run {
            observe(authErrorLiveData, ::handleAuthError)
            observe(errorResponseLiveData, ::handleResponseErrorUI)
            observe(errorLiveData, ::handleErrorUI)
            observe(isLoading, ::renderLoader)
        }
    }

    private fun renderLoader(isVisible: Boolean) {
        loader.setVisibleAlpha(isVisible)
    }

    private fun initLoader() {
        loader = DefaultFullscreenLoader(fragmentRootView.context)
        with(loader) {
            fragmentRootView.addView(this)
            this.addMatchParentFragmentLayoutParams()
            this.gone()
            this.bringToFront()
        }
    }

    private fun handleErrorUI(throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            val textToShow = "$ERROR_RESPONSE_TAG -> ${throwable.localizedMessage}"
            showToast(textToShow)
            Log.e(StringConstants.EYEZON_TAG, textToShow)
            throwable.printError()
        } else {
            showToast(getString(R.string.error_something_went_wrong))
        }
    }

    private fun handleResponseErrorUI(code: Int) {
        val textToShow = "$ERROR_RESPONSE_TAG -> $code"
        if (BuildConfig.DEBUG) {
            showToast(textToShow)
        }
        Log.e(StringConstants.EYEZON_TAG, textToShow)
    }

    protected fun addDisposable(disposable: Disposable) {
        disposableList.add(disposable)
    }

    private fun showToast(text: String) {
        Toast.makeText(activity, text, Toast.LENGTH_LONG).show()
    }

    protected fun showSnackbar(text: String) {
        Snackbar.make(fragmentRootView, text, Snackbar.LENGTH_LONG).show()
    }

    protected fun showSnackbar(textId: Int) {
        showSnackbar(getString(textId))
    }

    override fun onDestroy() {
        disposableList.forEach { disposable -> disposable.dispose() }
        super.onDestroy()
    }

    protected open fun handleNetworkStatus(connectionModel: ConnectionModel) {
        when (connectionModel.status) {
            ConnectionStatus.UNAVAILABLE -> showSnackbar(R.string.error_internet_connection_lost)
            ConnectionStatus.AVAILABLE -> handleNetworkAvailable()
        }
    }

    protected open fun handleNetworkAvailable() {

    }

    private fun handleAuthError(value: Boolean) {

    }
}