package com.witheyezon.eyezonbusiness.common.ui.tools.network

import android.app.Application
import android.content.Context
import android.net.*
import androidx.lifecycle.LiveData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkLiveData @Inject constructor(application: Application) : LiveData<ConnectionModel>() {

    private var connectivityManager: ConnectivityManager =
        application.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private var connectivityManagerCallback: ConnectivityManager.NetworkCallback =
        object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network?) {
                updateConnection()
            }

            override fun onLost(network: Network?) {
                updateConnection()
            }
        }

    private fun updateConnection() {
        val connectionType = getConnectionType()
        val isConnected =
            if (isConnected()) ConnectionStatus.AVAILABLE else ConnectionStatus.UNAVAILABLE

        postValue(ConnectionModel(connectionType, isConnected))
    }

    override fun onActive() {
        super.onActive()
        val builder = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        connectivityManager.registerNetworkCallback(builder.build(), connectivityManagerCallback)
    }

    override fun onInactive() {
        super.onInactive()
        connectivityManager.unregisterNetworkCallback(connectivityManagerCallback)
    }

    private fun isConnected(): Boolean {
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnected == true
    }

    private fun getConnectionType(): ConnectionType {
        var result =
            ConnectionType.UNKNOWN // Returns connection type. 0: none; 1: mobile data; 2: wifi

        connectivityManager.run {
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    result = ConnectionType.WIFI
                } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    result = ConnectionType.DATA
                }
            }
        }

        return result
    }
}