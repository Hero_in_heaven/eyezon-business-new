package com.witheyezon.eyezonbusiness.uikit.ext

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.witheyezon.eyezonbusiness.uikit.R

fun Context.showAlertDialog(
    title: String,
    message: String,
    positiveText: String,
    negativeText: String = "",
    dismissOnOk: Boolean = true,
    negativeButtonEnabled: Boolean = true,
    listener: ((DialogInterface) -> Unit)? = null
) {
    val dialogBuilder = AlertDialog.Builder(this, R.style.LogoutTheme)
        .setCancelable(false)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(positiveText) { dialog, _ ->
            if (dismissOnOk)
                dialog.dismiss()
            listener?.invoke(dialog)
        }

    if (negativeButtonEnabled)
        dialogBuilder.setNegativeButton(negativeText) { dialog, _ ->
            dialog.dismiss()
        }
    dialogBuilder.create().show()
}

fun Context.showAlertDialog(
    titleResId: Int,
    messageResId: Int,
    positiveTextResId: Int,
    negativeTextResId: Int,
    dismissOnOk: Boolean = true,
    negativeButtonEnabled: Boolean = true,
    listener: ((DialogInterface) -> Unit)? = null
) {
    showAlertDialog(
        getString(titleResId),
        getString(messageResId),
        getString(positiveTextResId),
        getString(negativeTextResId),
        dismissOnOk,
        negativeButtonEnabled,
        listener
    )
}