package com.witheyezon.eyezonbusiness.core.constants

object IntConstants {

    const val ZERO = 0
    const val ONE = 1
    const val MINUS_ONE = -1

}