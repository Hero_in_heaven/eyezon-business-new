package com.witheyezon.eyezonbusiness.common.ui.extensions

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.core.graphics.drawable.DrawableCompat
import com.witheyezon.eyezonbusiness.common.ui.BuildConfig
import com.witheyezon.eyezonbusiness.core.constants.LongConstants.DURATION_VIBRATION
import com.witheyezon.eyezonbusiness.core.constants.StringConstants.EYEZON_TAG

fun Activity.addToClipboard(text: String, toastText: String) {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("Eyezon business", text)
    clipboard.setPrimaryClip(clip)
    Toast.makeText(this, toastText, Toast.LENGTH_LONG).show()
}

fun ImageView.changeDrawableColor(color: Int) {
    DrawableCompat.setTint(DrawableCompat.wrap(this.drawable), color)
}

fun Context?.vibrate(duration: Long = DURATION_VIBRATION) {
    this?.apply {
        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v?.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            v?.vibrate(duration)
        }
    }
}