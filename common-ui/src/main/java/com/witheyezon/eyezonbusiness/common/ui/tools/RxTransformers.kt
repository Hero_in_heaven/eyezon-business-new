package com.witheyezon.eyezonbusiness.common.ui.tools

import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import org.reactivestreams.Subscription

object RxTransformers {

    fun applyCompletableBeforeAndAfter(before: Consumer<*>, after: Action): CompletableTransformer {
        return CompletableTransformer { upstream ->
            upstream
                .doOnDispose(after)
                .doOnSubscribe(before as Consumer<in Disposable>?)
                .doOnTerminate(after)
        }
    }

    fun <T> applyFlowableBeforeAndAfter(before: Consumer<Subscription>, after: Action): FlowableTransformer<T, T> {
        return FlowableTransformer { upstream ->
            upstream
                .doOnSubscribe(before)
                .doAfterTerminate(after)
        }
    }

    fun <T> applyMaybeBeforeAndAfter(before: Consumer<Disposable>, after: Action): MaybeTransformer<T, T> {
        return MaybeTransformer { upstream ->
            upstream
                .doOnSubscribe(before)
                .doAfterTerminate(after)
        }
    }

    fun <T> applyObservableBeforeAndAfter(before: Consumer<Disposable>, after: Action): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                .doOnDispose(after)
                .doOnSubscribe(before)
                .doOnTerminate(after)
        }
    }

    fun <T> applySingleBeforeAndAfter(before: Consumer<Disposable>, after: Action): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                .doOnDispose(after)
                .doOnSubscribe(before)
                .doAfterTerminate(after)
        }
    }
}