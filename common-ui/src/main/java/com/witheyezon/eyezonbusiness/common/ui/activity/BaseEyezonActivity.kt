package com.witheyezon.eyezonbusiness.common.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.witheyezon.eyezonbusiness.core.constants.IntConstants
import com.witheyezon.eyezonbusiness.core.constants.StringConstants
import com.witheyezon.eyezonbusiness.core.constants.StringConstants.ERROR_RESPONSE_TAG
import com.google.android.material.snackbar.Snackbar
import com.witheyezon.eyezonbusiness.common.ui.BuildConfig
import com.witheyezon.eyezonbusiness.common.ui.R
import com.witheyezon.eyezonbusiness.common.ui.extensions.observe
import com.witheyezon.eyezonbusiness.common.ui.tools.network.ConnectionModel
import com.witheyezon.eyezonbusiness.common.ui.tools.network.ConnectionStatus
import com.witheyezon.eyezonbusiness.common.ui.viewmodel.BaseEyezonViewModel
import com.witheyezon.eyezonbusiness.core.utils.printError
import com.witheyezon.eyezonbusiness.uikit.ext.addMatchParentLayoutParams
import com.witheyezon.eyezonbusiness.uikit.ext.gone
import com.witheyezon.eyezonbusiness.uikit.ext.setVisibleAlpha
import com.witheyezon.eyezonbusiness.uikit.view.loader.DefaultFullscreenLoader
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.Disposable

abstract class BaseEyezonActivity : DaggerAppCompatActivity() {

    private val disposableList = ArrayList<Disposable>()
    private val viewModel by lazy { viewModel() }
    private val activityRootView by lazy {
        window.decorView.rootView as ViewGroup
    }

    private lateinit var loader: DefaultFullscreenLoader

    abstract fun layoutId(): Int

    abstract fun viewModel(): BaseEyezonViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (layoutId() != IntConstants.ZERO)
            setContentView(layoutId())

        supportActionBar?.hide()
        changeStatusBarColor()
        viewModel.run {
            observe(errorResponseLiveData, ::handleResponseErrorUI)
            observe(errorLiveData, ::handleErrorUI)
            observe(isLoading, ::renderLoader)
            observe(authErrorLiveData, ::handleAuthError)
        }
    }

    private fun renderLoader(isVisible: Boolean) {
        loader.setVisibleAlpha(isVisible)
    }

    override fun onResume() {
        super.onResume()
        initLoader()
    }

    private fun initLoader() {
        loader = DefaultFullscreenLoader(this)
        with(loader) {
            activityRootView.addView(this)
            this.addMatchParentLayoutParams()
            this.gone()
            this.bringToFront()
        }
    }

    protected open fun handleErrorUI(throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            val textToShow = "$ERROR_RESPONSE_TAG -> ${throwable.localizedMessage}"
            showToast(textToShow)
            Log.e(StringConstants.EYEZON_TAG, textToShow)
            throwable.printError()
        } else {
            showToast(getString(R.string.error_something_went_wrong))
        }
    }

    protected open fun handleResponseErrorUI(code: Int) {
        val textToShow = "$ERROR_RESPONSE_TAG -> $code"
        if (BuildConfig.DEBUG) {
            showToast(textToShow)
        }
        Log.e(StringConstants.EYEZON_TAG, textToShow)
    }

    protected fun addDisposable(disposable: Disposable) {
        disposableList.add(disposable)
    }

    protected fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    protected fun showSnackbar(text: String) {
        Snackbar.make(activityRootView, text, Snackbar.LENGTH_LONG).show()
    }

    protected fun showSnackbar(textId: Int) {
        showSnackbar(getString(textId))
    }

    override fun onDestroy() {
        disposableList.forEach { disposable -> disposable.dispose() }
        viewModel.disposeAll()
        super.onDestroy()
    }

    private fun changeStatusBarColor(color: Int = R.color.blacked) {
        window.statusBarColor = ContextCompat.getColor(this, color)
    }

    private fun handleAuthError(value: Boolean) {

    }

    protected open fun handleNetworkStatus(connectionModel: ConnectionModel) {
        when (connectionModel.status) {
            ConnectionStatus.UNAVAILABLE -> handleNetworkUnavailable()
            ConnectionStatus.AVAILABLE -> handleNetworkAvailable()
        }
    }

    protected open fun handleNetworkUnavailable() {
        showSnackbar(R.string.error_internet_connection_lost)
    }

    protected open fun handleNetworkAvailable() {

    }
}