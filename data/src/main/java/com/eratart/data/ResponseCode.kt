package com.eratart.data

object ResponseCode {
    const val CODE_OK = 200
    const val CODE_400 = 400
    const val CODE_UNAUTHORIZED = 401
    const val CODE_500 = 500
}