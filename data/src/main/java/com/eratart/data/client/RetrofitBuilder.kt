package com.eratart.data.client

import com.eratart.data.client.headers.IHeadersProvider
import com.eratart.data.repository.auth.AuthService
import com.google.gson.GsonBuilder
import com.witheyezon.eyezonbusiness.core.constants.UrlConstants
import com.witheyezon.eyezonbusiness.core.utils.printError
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RetrofitBuilder @Inject constructor(private val headersProvider: IHeadersProvider) {

    companion object {
        private const val DEFAULT_TIMEOUT = 2000L
    }

    private val nonAuthHeaders = headersProvider.getNotAuthorizedHeaders()
    private val authHeaders = headersProvider.getNotAuthorizedHeaders()

    //private val baseUrl = if(BuildConfig.DEBUG) UrlConstants.BASE_URL else UrlConstants.BASE_URL_DEV
    private val baseUrl = UrlConstants.BASE_URL

    private fun <T> buildEyezonRetrofit(client: OkHttpClient, clazz: Class<T>) = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(
            GsonConverterFactory.create(GsonBuilder().create())
        )
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(client)
        .build()
        .create(clazz)

    private fun buildClient(headers: List<Pair<String, String>> = emptyList()): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
            .readTimeout(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
            .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
            .retryOnConnectionFailure(true)
        try {
            builder.addInterceptor { chain ->
                try {
                    val requestBuilder = chain.request().newBuilder()
                    headers.forEach { header ->
                        requestBuilder.addHeader(
                            header.first,
                            header.second
                        )
                    }
                    chain.proceed(requestBuilder.build())
                } catch (e: Exception) {
                    throw e
                }
            }
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        } catch (e: Exception) {
            e.printError()
        }
        return builder.build()
    }

    fun getAuthService() = buildEyezonRetrofit(buildClient(authHeaders), AuthService::class.java)

}