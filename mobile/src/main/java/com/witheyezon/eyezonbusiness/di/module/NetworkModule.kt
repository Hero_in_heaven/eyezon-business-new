package com.witheyezon.eyezonbusiness.di.module

import com.eratart.data.client.headers.HeadersProvider
import com.eratart.data.client.headers.IHeadersProvider
import dagger.Binds
import dagger.Module

@Module(
    includes = [
        ServiceModule::class,
        InteractorModule::class,
        RepositoryModule::class]
)
interface NetworkModule {

    @Binds
    fun bindHeadersProvider(provider: HeadersProvider): IHeadersProvider
}