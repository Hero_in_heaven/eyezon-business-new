package com.witheyezon.eyezonbusiness.uikit.view.button

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.witheyezon.eyezonbusiness.core.constants.IntConstants
import com.witheyezon.eyezonbusiness.core.constants.StringConstants
import com.witheyezon.eyezonbusiness.uikit.R
import kotlinx.android.synthetic.main.button_view.view.*
import java.lang.Exception

class EyezonButton(context: Context, attributeSet: AttributeSet? = null) : FrameLayout(context, attributeSet) {

    companion object {
        private const val TEXT_DEFAULT_VALUE = StringConstants.EMPTY
        private const val VALUE_ZERO = IntConstants.ZERO
    }

    init {
        inflate(getContext(), R.layout.button_view, this)
        applyButtonText(context, attributeSet)
    }

    private fun applyButtonText(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.EyezonButton, VALUE_ZERO, VALUE_ZERO)
        try {
            setButtonText(typedArray.getString(R.styleable.EyezonButton_button_text) ?: TEXT_DEFAULT_VALUE)
        } catch (e: Exception) {
           e.printStackTrace()
        }
    }

    private fun setButtonText(text: String) {
        tvEyezonButtonUikit.text = text
    }
}